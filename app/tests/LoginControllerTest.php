<?php

class LoginControllerTest extends TestCase
{
    public function testShowLogin()
    {
        $response = $this->action('GET', 'LoginController@showLogin');

        $this->assertResponseOk();
    }

    public function testLoginWithIncorrectDetails()
    {
        $response = $this->action('POST', 'LoginController@login');

        $this->assertRedirectedToRoute('login');

        $this->assertHasOldInput();

        $this->assertSessionHasErrors();
    }

    public function testLoginWithCorrectDetails()
    {
        $credentials = array(
            'email' => 'admin@licenseapp.co.uk',
            'password' => 'admin'
        );
        $response = $this->action('POST', 'LoginController@login', $credentials);

        $this->assertRedirectedTo('/');
    }

    public function testLogout()
    {
        $response = $this->action('GET', 'LoginController@logout');

        $this->assertRedirectedToRoute('login');
    }

    public function testShowLoginWhenAlreadyLoggedIn()
    {
        Route::enableFilters();

        $user = User::find(1);

        $this->be($user);

        $response = $this->action('GET', 'LoginController@showLogin');

        $this->assertRedirectedTo('/');
    }
}
 