@extends('layouts.master')

@section('title')
Login
@stop

@section('no-nav')
hidden
@stop

@section('no-footer')
hidden
@stop

@section('content')

    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <div class="page-header">
                <h1>Login</h1>
            </div>
            @include('layouts.partials.flash')
            @include('layouts.partials.form-errors')
            {{ Form::horizontal('login', $errors) }}
                {{ Form::emailField('email', 'Email', Input::old('email')) }}
                {{ Form::passwordField('password', 'Password') }}
                {{ Form::check('remember-me', 'Remember', 'yes') }}
                {{ Form::submitBtn('Login') }}
            {{ Form::close() }}
        </div>
    </div>

@stop