@if(Session::has('flash-message'))
    <div class="alert @if(Session::has('flash-type')) alert-{{ Session::get('flash-type') }} @else alert-success @endif">
        @if(Session::get('flash-dismiss')) <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> @endif
        {{ Session::get('flash-message') }}
    </div>
@endif