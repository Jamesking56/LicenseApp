<?php namespace LicenseApp\Alerts;

use Session;

class Alert
{
    private function alert($message, $type = 'success', $dismissable)
    {
        Session::flash('flash-message', $message);
        Session::flash('flash-type', $type);
        Session::flash('flash-dismiss', $dismissable);
    }

    public function info($message, $dismissable)
    {
        $this->alert($message, 'info', $dismissable);
    }

    public function warning($message, $dismissable)
    {
        $this->alert($message, 'warning', $dismissable);
    }

    public function danger($message, $dismissable)
    {
        $this->alert($message, 'danger', $dismissable);
    }

    public function success($message, $dismissable)
    {
        $this->alert($message, 'success', $dismissable);
    }
}
