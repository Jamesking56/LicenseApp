<?php namespace LicenseApp\Validation;

class UserValidator extends Validator
{
    // TODO: Update create rules.
    /**
     * Default Rules
     *
     * @var array
     */
    protected $rules = [
        'email' => 'required|email',
        'password' => 'confirmed'
    ];

    /**
     * Rules for updating a user.
     *
     * @var array
     */
    protected $updateRules = [
        'email' => 'required|email',
        'password' => 'confirmed'
    ];

    /**
     * Rules for logging in.
     *
     * @var array
     */
    public $loginRules = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    public function validateForLogin($input)
    {
        return $this->validate($input, $this->loginRules);
    }
}
