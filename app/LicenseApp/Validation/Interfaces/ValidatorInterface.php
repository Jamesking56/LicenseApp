<?php namespace LicenseApp\Validation\Interfaces;

interface ValidatorInterface
{
    /**
     * Perform validation
     *
     * @param $input
     * @param $rules
     *
     * @return bool
     * @throws \LicenseApp\Exceptions\ValidationException
     */
    public function validate($input, $rules);

    /**
     * Validate against default ruleset
     *
     * @param $input
     *
     * @return bool
     */
    public function validateForCreation($input);

    /**
     * Validate against update ruleset
     *
     * @param $input
     *
     * @return bool
     */
    public function validateForUpdate($input);
}
