<?php namespace LicenseApp\Validation; 

use Validator as V;
use LicenseApp\Exceptions\ValidationException;
use LicenseApp\Validation\Interfaces\ValidatorInterface;

abstract class Validator implements ValidatorInterface
{
    /**
     * Perform validation
     *
     * @param $input
     * @param $rules
     *
     * @return bool
     * @throws \LicenseApp\Exceptions\ValidationException
     */
    public function validate($input, $rules)
    {
        $validation = V::make($input, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation->errors());
        }

        return true;
    }

    /**
     * Validate against default ruleset
     *
     * @param $input
     *
     * @return bool
     */
    public function validateForCreation($input)
    {
        return $this->validate($input, $this->rules);
    }

    /**
     * Validate against update ruleset
     *
     * @param $input
     *
     * @return bool
     */
    public function validateForUpdate($input)
    {
        return $this->validate($input, $this->updateRules);
    }
}
