<?php namespace LicenseApp\Repositories\Interfaces;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface Repository
 * @package LicenseApp\Repositories\Interfaces
 */
interface RepositoryInterface
{
    /**
     * @return Collection
     */
    public function all();

    /**
     * @return Eloquent
     */
    public function getModel();

    /**
     * @param Eloquent $model
     * @return mixed
     */
    public function setModel(Eloquent $model);

    /**
     * @param $id
     * @return Eloquent
     */
    public function find($id);
}
