<?php namespace LicenseApp\Repositories;

use Auth;
use Hash;
use Illuminate\Database\Eloquent\Collection;
use LicenseApp\Exceptions\ValidationException;
use LicenseApp\Repositories\Interfaces\UserRepositoryInterface;
use LicenseApp\Validation\UserValidator;
use User;

/**
 * Class UserRepository
 * @package LicenseApp\Repositories
 */
class UserRepository extends Repository implements UserRepositoryInterface
{

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Login the User. (No Validation)
     *
     * @param $email    string  User's Email Address
     * @param $password string  User's Password (not encrypted)
     * @param $remember boolean Should we remember the User?
     * @return boolean  Was the User was logged in successfully?
     */
    public function login($email, $password, $remember = false)
    {
        return Auth::attempt(['email' => $email, 'password' => $password], $remember);
    }

    /**
     * Register a new User. (No Validation)
     *
     * @param $email    string  User's Email Address
     * @param $password string  User's Password (not encrypted)
     * @param $firstname string User's First Name.
     * @param $lastname string  User's Last Name.
     * @return  boolean Was the User registered successfully?
     */
    public function register($email, $password, $firstname, $lastname)
    {
        // Create User Account
        $user = $this->model->create([
            'email' => $email,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname
        ]);
    }

    /**
     * Logout the User.
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
    }

    /**
     * Login as a specific User (For Admin access).
     *
     * @param $id   integer The ID of the User to login as.
     * @return boolean  Was the Admin logged in as the User successfully?
     */
    public function loginAsId($id)
    {
        return Auth::loginUsingId($id);
    }

    /**
     * Request an Invite (Private Beta)
     *
     * @param $email    string  The Email to request as.
     * @param $password string  The Password for the account to be created with (not encrypted).
     * @return boolean  Was an invite requested successfully?
     */
    public function requestInvite($email, $password)
    {
        // TODO: Implement requestInvite() method.
    }

    /**
     * Find a User by their Email.
     *
     * @param $email    string  The Email Address to search for.
     * @return boolean  The User which was found or false.
     */
    public function findByEmail($email)
    {
        // TODO: Implement findByEmail() method.
    }

    /**
     * Get all inactive Users.
     *
     * @return Collection   List of currently inactive Users.
     */
    public function getInactive()
    {
        // TODO: Implement getInactive() method.
    }

    /**
     * Get all suspended Users.
     *
     * @return Collection   List of currently suspended Users.
     */
    public function getSuspended()
    {
        // TODO: Implement getSuspended() method.
    }

    /**
     * @param $code
     * @return mixed
     */
    public function registerFromInvite($code)
    {
        // TODO: Implement registerFromInvite() method.
    }
}
