<?php namespace LicenseApp\Repositories;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use LicenseApp\Repositories\Interfaces\RepositoryInterface;

/**
 * Class Repository
 * @package LicenseApp\Repositories
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var Eloquent
     */
    protected $model;

    /**
     * @return Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @return Eloquent
     */
    public function getModel()
    {
        return $this->model();
    }

    /**
     * @param Eloquent $model
     * @return mixed
     */
    public function setModel(Eloquent $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return Eloquent
     */
    public function find($id)
    {
        $result = null;
        try {
            $result = $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {

        }

        return $result;
    }
}
