<?php namespace LicenseApp\HTML;

use Illuminate\Html\FormBuilder as IlluminateFormBuilder;

class FormBuilder extends IlluminateFormBuilder
{
    protected $errors;

    public function textField($name, $label, $value = null, $options = [])
    {
        return $this->field('text', $name, $label, $value, $options);
    }

    public function emailField($name, $label, $value = null, $options = [])
    {
        return $this->field('email', $name, $label, $value, $options);
    }

    public function passwordField($name, $label, $options = [])
    {
        return $this->field('password', $name, $label, null, $options);
    }

    public function horizontal($url, $errors, $method = 'POST')
    {
        $this->errors = $errors;

        $options = array('url' => $url, 'method' => $method, 'class' => 'form-horizontal', 'role' => 'form');

        return $this->open($options);
    }

    public function submitBtn($value, $options = [])
    {
        $options = array_merge(array('class' => 'btn btn-primary', 'name' => 'submit'), $options);

        return $this->submit($value, $options);
    }

    public function check($name, $label, $value, $checked = false, $options = [])
    {
        $options = array_merge(['class' => 'form-control'], $options);

        if($checked) $options['checked'] = 'checked';

        return $this->field('checkbox', $name, $label, $value, $options);
    }

    private function inputWrap($html, $field)
    {
        if ($this->errors->has($field))
        {
            $html .= "<span class=\"glyphicon glyphicon-warning-sign form-control-feedback\"></span>";
        }
        return "<div class=\"col-sm-10\">{$html}</div>";
    }

    private function wrap($html, $field)
    {
        $error = "";
        if ($this->errors->has($field))
        {
            $error = " has-warning has-feedback";
        }
        return "<div class=\"form-group{$error}\">{$html}</div>";
    }

    private function field($type, $name, $label, $value, $options)
    {
        $options = array_merge(['class' => 'form-control', 'placeholder' => $label], $options);

        $label = $this->label($name, ucwords($label), ['class' => 'col-sm-2 control-label', 'for' => $name]);
        $input = $this->input($type, $name, $value, $options);
        $input = $this->inputWrap($input, $name);

        $html = $label . $input;

        return $this->wrap($html, $name);
    }
}
