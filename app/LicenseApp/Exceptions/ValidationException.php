<?php namespace LicenseApp\Exceptions;

use Exception;
use LicenseApp\Exceptions\Interfaces\ValidationExceptionInterface;

class ValidationException extends Exception
{
    protected $errors;

    public function __construct($errors, $message = null, $code = 0, Exception $previous = null)
    {
        $this->errors = $errors;

        parent::__construct($message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
