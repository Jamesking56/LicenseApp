<?php namespace LicenseApp\Mailers;

use Mail;

abstract class Mailer
{
    protected function sendTo($user, $subject, $view, $data = [])
    {
        Mail::queue($view, $data, function ($message) use ($user, $subject) {
            $message->to($user->email)
                    ->subject($subject);
        });

        return true;
    }
}
