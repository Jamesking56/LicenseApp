<?php namespace LicenseApp\Mailers;

use User;

class UserMailer extends Mailer
{
    public function welcome(User $user)
    {
        $view = 'emails.welcome'; // TODO: Create emails.welcome view
        $data = [];
        $subject = 'Welcome to LicenseApp';

        return $this->sendTo($user, $subject, $view, $data);
    }

    public function cancellation(User $user)
    {
        $view = 'emails.cancel'; // TODO: Create emails.cancel view
        $data = [];
        $subject = 'Sorry to see you leave';

        return $this->sendTo($user, $subject, $view, $data);
    }
}
