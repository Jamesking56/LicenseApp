<?php namespace LicenseApp;

use Illuminate\Support\ServiceProvider;
use LicenseApp\HTML\FormBuilder;

class LicenseAppServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFacades();

        $this->registerRepos();
    }

    protected function registerRepos()
    {
        $this->app->bind(
            'LicenseApp\Repos\Interfaces\UserRepositoryInterface',
            'LicenseApp\Repos\UserRepository'
        );
    }

    protected function registerFacades()
    {
        $this->app->bind('alert', 'LicenseApp\Alerts\Alert');
    }
}
