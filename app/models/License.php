<?php

use Laracasts\Presenter\Contracts\PresentableInterface;
use Laracasts\Presenter\PresentableTrait;

class License extends Eloquent implements PresentableInterface {

    use PresentableTrait;

    protected $presenter = 'LicenseApp\Presenters\License';

	protected $guarded = array();

    protected $softDelete = true;

    protected $dates = ['deleted_at', 'expires_at'];

}
