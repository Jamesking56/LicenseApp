<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicenseTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('license_types', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->string('name');
			$table->text('description')->nullable();
			$table->string('prefix')->nullable();
			$table->string('suffix')->nullable();
			$table->integer('length')->default(20);
			$table->boolean('lock_ip')->default(0);
			$table->boolean('lock_domain')->default(0);
			$table->boolean('lock_directory')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('license_types');
	}

}
