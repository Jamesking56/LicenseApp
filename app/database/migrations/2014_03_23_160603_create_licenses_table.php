<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licenses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('type_id')->unsigned();
			$table->string('code');
			$table->enum('status', array('Active', 'Expired', 'Suspended'))->default('Active');
            $table->timestamp('expires_at')->nullable();
			$table->timestamps();

            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licenses');
	}

}
