<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefineForeignKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('license_types', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::table('licenses', function(Blueprint $table){
            $table->foreign('type_id')->references('id')->on('license_types')->onDelete('cascade');
        });

        Schema::table('api_keys', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::table('audit_log', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('products', function(Blueprint $table){
            $table->dropForeign('products_user_id_foreign');
        });

        Schema::table('license_types', function(Blueprint $table){
            $table->dropForeign('license_types_product_id_foreign');
        });

        Schema::table('licenses', function(Blueprint $table){
            $table->dropForeign('licenses_type_id_foreign');
        });

        Schema::table('api_keys', function(Blueprint $table){
            $table->dropForeign('api_keys_product_id_foreign');
        });

        Schema::table('audit_log', function(Blueprint $table){
            $table->dropForeign('audit_log_user_id_foreign');
        });
	}

}
