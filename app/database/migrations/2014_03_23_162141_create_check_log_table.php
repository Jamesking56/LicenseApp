<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('check_log', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->timestamp('timestamp');
			$table->text('response');
			$table->boolean('valid')->default(false);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('check_log');
	}

}
