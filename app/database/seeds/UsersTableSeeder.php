<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
        Eloquent::unguard();

        User::create([
            'email' => 'admin@licenseapp.co.uk',
            'password' => Hash::make('admin'),
            'first_name' => 'Admin',
            'last_name' => 'User',
            'rank' => 2,
            'activated' => true
        ]);

        User::create([
            'email' => 'test@licenseapp.co.uk',
            'password' => Hash::make('test'),
            'first_name' => 'Test',
            'last_name' => 'User',
            'rank' => 1,
            'activated' => true
        ]);

        $faker = Faker\Factory::create();

        foreach(range(1, 18) as $index)
        {
            User::create([
                'email' => $faker->safeEmail,
                'password' => Hash::make($faker->word),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'rank' => 1,
                'activated' => $faker->boolean(),
                'banned' => $faker->boolean()
            ]);
        }
	}

}
