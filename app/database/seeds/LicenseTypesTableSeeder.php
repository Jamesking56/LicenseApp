<?php

class LicenseTypesTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            LicenseType::create([
                'product_id' => $faker->randomNumber(1, 20),
                'name' => $faker->word,
                'description' => $faker->text(),
                'prefix' => $faker->word . "-",
                'suffix' => "-" . $faker->word
            ]);
        }
    }

}
