<?php

class CheckLogTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            CheckLog::create([
                'code' => $faker->md5,
                'timestamp' => $faker->unixTime,
                'response' => $faker->text(),
                'valid' => $faker->boolean()
            ]);
        }
    }

}
