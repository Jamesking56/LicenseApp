<?php

class LicensesTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            License::create([
                'type_id' => $faker->randomNumber(1, 20),
                'code' => $faker->md5,
                'status' => $faker->randomElement(array('Active', 'Expired', 'Suspended'), 1)
            ]);
        }
    }

}
