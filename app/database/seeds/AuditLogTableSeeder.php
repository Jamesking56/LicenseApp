<?php

class AuditLogTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            AuditLog::create([
                'user_id' => $faker->randomNumber(1, 20),
                'type' => $faker->randomElement(array('User', 'Admin')),
                'action' => $faker->sentence,
                'notes' => $faker->text
            ]);
        }
    }

}
