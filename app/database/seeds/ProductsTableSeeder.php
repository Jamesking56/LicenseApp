<?php

class ProductsTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            Product::create([
                'user_id' => $faker->randomNumber(1, 20),
                'name' => $faker->word,
                'description' => $faker->text()
            ]);
        }
    }

}
