<?php

class ApiKeysTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        $faker = Faker\Factory::create();

        foreach(range(1, 20) as $index)
        {
            ApiKey::create([
                'product_id' => $faker->randomNumber(1, 20),
                'apikey' => $faker->uuid
            ]);
        }
    }

}
