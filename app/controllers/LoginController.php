<?php

use LicenseApp\Exceptions\ValidationException;
use LicenseApp\Repositories\UserRepository;
use LicenseApp\Validation\UserValidator;

class LoginController extends \BaseController {

    protected $validator;

    protected $repo;

    function __construct(UserValidator $validator, UserRepository $repo)
    {
        $this->validator = $validator;
        $this->repo = $repo;
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

    public function showLogin()
    {
        return View::make('login');
    }

    public function login()
    {
        try {
            $this->validator->validateForLogin(Input::all());
        } catch(ValidationException $e) {
            return Redirect::to('login')->withErrors($e->getErrors())->withInput();
        }

        if ($this->repo->login(Input::get('email'), Input::get('password'), Input::get('remember-me') == "yes" ? true : false))
        {
            return Redirect::intended('/');
        }

        Alert::danger('Incorrect email / password combination.', false);
        return Redirect::route('login')->withInput();
    }

    public function logout()
    {
        Auth::logout();
        Alert::info('You have been logged out successfully.', true);
        return Redirect::route('login');
    }

}