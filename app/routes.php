<?php

Route::group(array('domain' => 'api.licenseapp.co.uk'), function(){

});

Route::group(array('domain' => 'admin.licenseapp.co.uk'), function(){

});

Route::group(array('domain' => 'dashboard.licenseapp.co.uk'), function(){
    Route::get('/', function(){
        return 'home';
    });
    Route::get('login', ['as' => 'login', 'uses' => 'LoginController@showLogin'])->before('guest');
    Route::post('login', ['as' => 'login.post', 'uses' => 'LoginController@login'])->before('guest');
    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout'])->before('auth');
});

Route::get('/', function(){
    return Redirect::to('http://dashboard.licenseapp.co.uk');
});