<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * ApiKey
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $apikey
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\ApiKey whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\ApiKey whereProductId($value) 
 * @method static \Illuminate\Database\Query\Builder|\ApiKey whereApikey($value) 
 * @method static \Illuminate\Database\Query\Builder|\ApiKey whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\ApiKey whereUpdatedAt($value) 
 */
	class ApiKey {}
}

namespace {
/**
 * AuditLog
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $type
 * @property string $action
 * @property string $notes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereType($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereAction($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereNotes($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\AuditLog whereUpdatedAt($value) 
 */
	class AuditLog {}
}

namespace {
/**
 * CheckLog
 *
 * @property integer $id
 * @property string $code
 * @property string $timestamp
 * @property string $response
 * @property boolean $valid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereTimestamp($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereResponse($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereValid($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\CheckLog whereUpdatedAt($value) 
 */
	class CheckLog {}
}

namespace {
/**
 * License
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $code
 * @property string $status
 * @property \Carbon\Carbon $expires_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\License whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereTypeId($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereStatus($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereExpiresAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\License whereDeletedAt($value) 
 */
	class License {}
}

namespace {
/**
 * LicenseType
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property string $description
 * @property string $prefix
 * @property string $suffix
 * @property integer $length
 * @property boolean $lock_ip
 * @property boolean $lock_domain
 * @property boolean $lock_directory
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereProductId($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereDescription($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType wherePrefix($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereSuffix($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereLength($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereLockIp($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereLockDomain($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereLockDirectory($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\LicenseType whereUpdatedAt($value) 
 */
	class LicenseType {}
}

namespace {
/**
 * Product
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Product whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Product whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Product whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\Product whereDescription($value) 
 * @method static \Illuminate\Database\Query\Builder|\Product whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Product whereUpdatedAt($value) 
 */
	class Product {}
}

namespace {
/**
 * Class User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property integer $rank
 * @property boolean $activated
 * @property boolean $banned
 * @property string $last_login
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property boolean $stripe_active
 * @property string $stripe_id
 * @property string $stripe_plan
 * @property string $last_four
 * @property \Carbon\Carbon $trial_ends_at
 * @property \Carbon\Carbon $subscription_ends_at
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereFirstName($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereLastName($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereRank($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereActivated($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereBanned($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereLastLogin($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereStripeActive($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereStripeId($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereStripePlan($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereLastFour($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereTrialEndsAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereSubscriptionEndsAt($value) 
 */
	class User {}
}

